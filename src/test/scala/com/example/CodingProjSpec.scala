package com.example

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import CodingProj._

case class MovieStar(name: String)
class CodingProjSpec extends AnyWordSpec with Matchers{
  "tailrec reverse" when {
    "invoked with a list of elements" should {
      "return a reversed list of ints" in {
        listReverse(List(1, 2, 3, 4, 5, 6)) shouldBe List(6, 5, 4, 3, 2, 1)
      }
      "return a reversed list of objects" in {
        listReverse(
          List(
            MovieStar("Sean"),
            MovieStar("Roger"),
            MovieStar("Timothy"),
            MovieStar("Pierce"),
            MovieStar("Daniel")
          )
        ) shouldBe List(
          MovieStar("Daniel"),
          MovieStar("Pierce"),
          MovieStar("Timothy"),
          MovieStar("Roger"),
          MovieStar("Sean")
        )
      }
    }
  }
  "Consecutive Mapper" when {
    "invoked with an empty or blank sting" should {
      "return an empty string" in {
        consecutiveCount("") shouldBe Right("")
        consecutiveCount("\t  \t\n") shouldBe Right("")
      }
    }
    "invoked with a mix of letters and invalid characters" should {
      "return an error on the first number" in {
        consecutiveCount("ABCDabbef1") shouldBe Left(ConsecutiveError('1'))
      }
      "return an error on the first blank" in {
        consecutiveCount("bbbcccdd\t123") shouldBe Left(ConsecutiveError('\t'))
      }
    }
    "invoked with only letters" should {
      "return the input string" when {
        "all the letters are non-consecutive" in {
          val inputString = "BACARabBdD"
          consecutiveCount(inputString) shouldBe Right(inputString)
        }
      }
      "return a proper mapping of the letters" in {
        consecutiveCount("AAAAABBBCCC") shouldBe Right("5A3B3C")
        consecutiveCount("AAAAABCDDD") shouldBe Right("5ABC3D")
      }
    }
  }
}
