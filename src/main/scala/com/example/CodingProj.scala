package com.example

import scala.annotation.tailrec

object CodingProj {
  case class ConsecutiveError(c: Char)
  def listReverse[A](inputList: List[A]): List[A] = {
    def listReverseHelper[A](inputList: List[A], accum: List[A]): List[A] = {
      inputList match {
        case Nil => accum
        case init :: rest => listReverseHelper(rest, init::accum)
      }
    }
    listReverseHelper(inputList, Nil)
  }

  def consecutiveCount(input: String): Either[ConsecutiveError, String] = {
    @tailrec
    def consecutiveHelper(input: List[Char], accum: Either[ConsecutiveError, List[(Char, Long)]]): Either[ConsecutiveError, List[(Char, Long)]] = {
      (input, accum) match {
        case (_, err @ Left(_)) => err
        case (Nil, accumListEither) => accumListEither
        case (h :: rest, Right((topChar, count) :: bottom) ) if h.isLetter && topChar == h =>
          consecutiveHelper(rest, Right((topChar, count + 1) :: bottom))
        case (h :: rest, Right(accumList)) if h.isLetter =>
          consecutiveHelper(rest, Right((h, 1L) :: accumList))
        case (h :: _, _) => Left(ConsecutiveError(h))
      }
    }
    consecutiveHelper(input.trim.toCharArray.toList, Right(List.empty[(Char, Long)])).map { tupleList =>
      tupleList.reverse.map {case (letter, count) =>
        if(count == 1) letter.toString()
        else s"$count$letter"
      }.mkString("")
    }
  }
}
