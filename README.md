# HCL Questions and answers
Requires Java 8+  

The code for the tests is located in `/src/main/scala/com/example/CodingProj`

The testing specs are located in `src/test/scala/com/example/CodingProjSpec`

To run the unit tests from the command line execute:

### Windows
```
sbt.bat test
```

### Mac/Linux
```
./sbt test
```